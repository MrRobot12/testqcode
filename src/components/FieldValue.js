import React ,{Component} from 'react';


class FieldValue extends Component {
    render() {
        var NumberFormat = require('react-number-format');
        return(
            <NumberFormat thousandSeparator="." decimalSeparator="," decimalScale={2}  onChange={this.props.onChange} value={this.props.value} id={this.props.id} name={this.props.name} className="form-control"/>
        );
    }
}



export default FieldValue;