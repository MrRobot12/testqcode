import React ,{Component} from 'react';
import FieldValue from './FieldValue';
import axios from 'axios';
import './../App.css';

class Form extends Component{
    constructor() {
        super();
        this.state = {
            value:"",
            description:'',
            TRM:"",
            message:''
        };
        this.handleInputChange= this.handleInputChange.bind(this);
        this.HandleSave = this.HandleSave.bind(this);
        this.HandleReset = this.HandleReset.bind(this);
    };

    handleInputChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    };

    HandleSave (e) {
        if(!this.HandleValidate()){
            return;
        }
        const camps = {
            value: this.state.value,
            description: this.state.description,
            TRM: this.state.TRM
        }
        axios.post('https://httpbin.org/post ',{camps})
        .then(res => {
            console.log(res);
            var cont=localStorage.length;
            cont= cont+1;
            localStorage.setItem(cont, JSON.stringify(camps));
            alert('Se Guardo Correctamente  ')
            window.location.reload();
        })
        this.setState({
        }
        )
    }
    HandleSubmit(e) {
        if(!this.HandleValidate()){
        }
    }

    HandleValidate(){
        if(this.state.value === '' || this.state.description === '' || this.state.TRM === ''  ){
            alert('Todos los campos tienen que estar Completos  ')
            return false
        }
        return true
    }
    HandleReset = () => {
        document.getElementById('value').value = '';
        document.getElementById('description').value = '';
        document.getElementById('TRM').value = '';
        this.setState({
            ['value']: '',
            ['description']: '',
            ['TRM']: '',
            ['message']: ''
        })
    }
    render() {
        return (
            <div>
                <div className="form-group">
                    <label htmlFor="value">Valor</label>
                    <FieldValue id="value" name="value" value={this.state.value} onChange={this.handleInputChange}/>
                </div>
                <div className="form-group">
                    <label htmlFor="description">descripcion</label>
                    <select className="form-control" id="description" name="description" onChange={this.handleInputChange}>
                        <option value="">Seleccione</option>
                        <option value="item1">Descripcion  Item 1</option>
                        <option value="item2">Descripcion  Item 2</option>
                        <option value="item3">Descripcion  Item 3</option>
                        <option value="item4">Descripcion  Item 4</option>
                        <option value="item5">Descripcion  Item 5</option>
                    </select>
                </div>
                <div className="form-group">
                    <label htmlFor="TRM">TRM</label>
                    <FieldValue id="TRM" name="TRM" value={this.state.TRM} onChange={this.handleInputChange}></FieldValue>
                </div>
                <button onClick={this.HandleSave} className="btn btn-success margin">Guardar</button>
                <button onClick={this.HandleReset} className="btn btn-dark">Limpiar</button>
                </div>
        );
    }
}

export default Form;