import React ,{Component} from 'react';
import ReactTable from 'react-table';
import 'react-table/react-table.css';

class ListForm extends Component {
    constructor() {
        super();
    }
    render() {
      var Data = []
      for (var i=1; i<localStorage.length + 1; i++){
        Data.push(JSON.parse(localStorage.getItem(i)));
      }
      const columns = [{
        title: 'value', dataIndex: 'value', key:'value', width: 150,
        }, {
        title: 'description', dataIndex: 'description', key:'description', width: 150,
        }, {
        title: 'TRM', dataIndex: 'TRM', key:'TRM', width: 150,
        }
      ];

      const column = [{
        Header: 'value',
        accessor: 'value'
        }, {
        Header: 'description',
        accessor: 'description'
        }, {
        Header: 'TRM',
        accessor: 'TRM'
        }
      ]
      return (
        <ReactTable data={Data} columns={column} defaultPageSize={10} ></ReactTable>
      );
    }
}
export default ListForm;