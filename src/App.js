import React from 'react';
import Form from './components/Form'
import ListForm from './components/ListForm'
import './App.css';
function App() {
  return (
    <div className="App">
      <div className="container mt-5">
        <div className="row">
          <div className="col-md-4" role="alert">
            <div className="alert alert-success align mt-3">¡¡Formulario!!</div>
            <div className="alert alert-secondary align mt-2"><Form></Form></div>
            </div>
            <div className="col-md-6 offset-1 fondo align">
            <ListForm/>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
